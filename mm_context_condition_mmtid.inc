<?php

/**
 * Expose paths as a context condition.
 */
class mm_context_condition_mmtid extends context_condition {
  /**
   * Omit condition values. We will provide a custom input form for our conditions.
   */
  function condition_values() {
    return array();
  }

  /**
   * Condition form.
   */
  function condition_form($context) {
    $form = parent::condition_form($context);
    unset($form['#options']);

    $form['#type'] = 'mm_catlist';
    $form['#mm_list_selectable'] = MM_PERMS_READ;
    $form['#default_value'] = $this->fetch_from_context($context, 'values');
    return $form;
  }

  /**
   * Condition form submit handler.
   */
  function condition_form_submit($values) {
    if (is_array($values)) {
      return $values;
    }

    $ret = array();
    $matches = array();
    preg_match_all('/(\d+)\{([^}]+)\}/', $values, $matches);
    if (!empty($matches[1]) && !empty($matches[2])) {
      $ret = array_combine($matches[1], $matches[2]);
    }

    return $ret;
  }

  /**
   * Execute.
   */
  function execute() {
    if ($this->condition_used()) {
      // Include both the path alias and normal path for matching.
      $current_path = array(drupal_get_path_alias($_GET['q']));
      if ($current_path[0] != $_GET['q']) {
        $current_path[] = $_GET['q'];
      }
      foreach ($this->get_contexts() as $context) {
        $paths = $this->fetch_from_context($context, 'values');
        if ($this->match($current_path, $paths, TRUE)) {
          $this->condition_met($context);
        }
      }
    }
  }

  /**
   * Match the current path to an array of Monster Menus Tree IDs.
   *
   * @param mixed $paths
   *   The path string or an array of strings to be matched.
   * @param array $mmtids
   *   An array of Monster Menus Tree IDs. If any of the MMTIDs are the page
   *   associated with the input path, or a parent of that path, the condition
   *   will be satisfied.
   * @return boolean
   *   Whether the path is a page in the MMTIDs array, or a child of one of those
   *   pages.
   */
  protected function match($paths, $mmtids) {
    $paths = !is_array($paths) ? array($paths) : $paths;

    foreach ($paths as $path) {
      $matches = array();
      preg_match('/mm\/(\d+)/', $path, $matches);
      if (!empty($matches[1])) {
        $parents = mm_content_get_parents_with_self($matches[1]);
        foreach (array_keys($mmtids) as $mmtid) {
          if (in_array($mmtid, $parents)) {
            return TRUE;
          }
        }
      }
    }

    return FALSE;
  }
}
